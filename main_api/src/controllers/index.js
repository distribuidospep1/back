import { Router } from 'express';
import formController from './form.js';

const router = Router();

//Example
router.use('/form', formController);

export default router;
