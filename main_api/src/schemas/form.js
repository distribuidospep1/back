import Sequelize from 'sequelize';
import { sequelize } from './index.js'
import EncryptedField from 'sequelize-encrypted'
const {DataTypes} = Sequelize;

var key = process.env.SECRETE_KEY_DB;
var enc_fields = EncryptedField(Sequelize, key);

const Form = sequelize.define('form', {
encrypted: enc_fields.vault('encrypted'),
id:{
   type:Sequelize.INTEGER,
   primaryKey:true,
   autoIncrement: true,
},
rut: enc_fields.field('rut',{
    type: Sequelize.STRING,
}),
names: enc_fields.field('names',{
    type: Sequelize.STRING,
}),
last_name: enc_fields.field('last_name',{
    type: Sequelize.STRING,
}),
region_name: enc_fields.field('region_name',{
    type: Sequelize.STRING,
}),
commune: enc_fields.field('commune',{
    type: Sequelize.STRING,
}),
address: enc_fields.field('address',{
    type: Sequelize.STRING,
}),
street_number: enc_fields.field('street_number',{
    type: Sequelize.STRING,
}),
reason: enc_fields.field('reason',{
    type: Sequelize.STRING,
}),
email: enc_fields.field('email',{
    type: Sequelize.STRING,
}),
validation_date:{
    type: DataTypes.DATE 
},
expiration_date:{
    type: DataTypes.DATE 
},
},{
    timestamps: true,
    createdAt: true,
    updatedAt: false,
    freezeTableName: true
});

Form.sync() // Create the table if not exit in DB

//Form.sync({force:true}) // Drop and create the table

export default Form;