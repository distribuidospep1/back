import Sequelize from 'sequelize';

export const sequelize = new Sequelize('distribuidos', 'postgres', 'postgres', {
  host: "postgres",
  dialect: 'postgres',
  port:5432,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  logging: false
});

sequelize.authenticate().then(() => {
  console.log('Connection has been established successfully.');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
});

export default sequelize;