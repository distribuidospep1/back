import { Router } from 'express';
import validationController from './validation.js';

const router = Router();

//Example
router.use('/validation', validationController);

export default router;
