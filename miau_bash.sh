#!/bin/bash

while getopts n: flag
do
    case "${flag}" in
        n) n=${OPTARG};;
    esac
done

for ((times=1;times<=$n;times++))
do

curl -i -X POST -H "Content-Type: application/json" -d "{\"rut\":\"10.000.000-2\",\"names\":\"Juanito\",\"last_name\":\"Perez\",\"region\":\"Santiago\",\"commune\":\"Maipu\",\"address\":\"Baquedano\",\"street_number\":\"123\",\"reason\":\"comprar\",\"email\":\"test@test.cl\"}" http://localhost:3000/form/create

done

echo $'\n\n ** Comisaría Kawaii ha enviado todos los correos del universo gatuno :3 **'

