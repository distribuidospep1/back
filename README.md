# Para poder ejecutar este proyecto siga las siguientes instrucciones:

### Ubíquese en la carpeta de back
```
...\back
```

### Teniendo instalado docker y docker-compose en su computador ejecutar
```
docker-compose up 
```

### Instrucciones para utilizar script bash
```
* Este script está pensado y desarrollado en un entorno Linux Ubuntu *

1- Abrir la terminal de Ubuntu y dirigirse al directorio donde se encuentra el archivo 'miau_bash.sh'.
2- Escribir en la terminal: './miau_bash.sh -n XX', siendo XX el número de veces que se quiere ejecutar la petición POST.
3- Para saber el tiempo que demora en ejecutarse este script se puede escribir: 'time ./miau_bash.sh -n XX', donde el parámetro 'real' indicará el tiempo total que tuvo la ejecución de las N consultas.
```

# Características de un SD (sistema distribuido):

1- Heterogeneidad: Los SD son construidos desde una gran variedad de componentes, equipos, sistemas operativos, etc. Estas comunicaciones a través de un middleware debe poder tratar las diferencias y comunicación entre estos componentes.

2- Apertura (openess): Se refiere a la capacidad de un SD de extenderse o expandirse respecto a extensiones físicas (memoria, periféricos, etc.) o de software (protocolos, servicios de compartición de recursos, etc.) para la implementación de nuevos servicios o reformulación de los antiguos, sin perjudicar el sistema original.

3- Seguridad: Referido mayormente a técnicas de encriptación, ya que al tener recursos compartidos y transmitirse mediante la red, la información es susceptible.

4- Escalabilidad: Implica que cualquier recurso pueda extenderse para suministrar el servicio de la escala de usuarios que se solicite. Es decir, si la demanda de un servicio aumenta considerablemente, también debería aumentar la extensión del sistema para dar dicho servicio. Algunas técnicas para solucionar esto son: replicación de datos, caching, uso de varios servidores.

5- Manejo de fallos: Al ocurrir un error, ya sea de software o hardware, el SD debe ser capaz de recuperarse (roll-back) de un fallo y ejecutar sin complicaciones operaciones críticas. Una forma de medida de esto es mediante la disponibilidad, lo cual se mide como la proporción de tiempo en que el sistema está disponible para su uso.

6- Concurrencia: Un SD permite esta característica cuando tiene recursos capaces de ser compartidos entre distintos clientes al mismo tiempo. Para permitir esto las operaciones son sincronizadas (semáforos, lock) para mantener la consistencia de los datos.

7- Transparencia: Encubrimiento de la separación de los componentes de un SD tanto a un usuario como al programador, de manera de que el sistema sea percibido como un todo.
        a) Transparencia de acceso: accede a objetos remotos de la misma forma que a los locales.
        b) Transparencia de localización: permite el acceso a objetos sin conocimiento exacto de su ubicación.
        c) Transparencia de concurrencia: permite la ejecución de varios procesos que utilicen el mismo objeto de forma que no interfieran entre ellos.
        d) Transparencia de replicación: permite el uso de múltiples instancias de objetos replicados sin que se informe que no es de procedencia original.
        e) Transparencia de fallos: permite concluir tareas del sistema a pesar de la ocurrencia de un fallo, sin que el usuario lo presencie.
        f) Transparencia de migración: permite el movimiento de objetos sin afectar la disponibilidad del sistema o el objeto en sí.
        g) Transparencia de prestaciones: permite la reconfiguración del sistema para administrar la carga sin afectar la disponibilidad del sistema.
        h) Transparencia de escalado: permite la expansión del sistema sin cambiar o dañar la estructura del mismo.

*Entiéndase por objeto: servicio, información, aplicación, etc.*

8- QoS (quality of service): Capacidad de no solamente realizar la entrega de un servicio, sino que este sea entregado de la mejor forma, cumpliendo parámetros de rendimiento, seguridad y fiabilidad tanto del servicio como de la información.

# Características que cumple "Comisaría Kawaii" de un SD (sistema distribuido):

1- No lo cumple, ya que el sistema es solamente local. Por lo tanto no se cuenta con un middleware que haga el rol de comunicación de componentes, ya que solamente se tiene uno.

2- No lo cumple, ya que el sistema es solamente local. Por el lado de los componentes físicos no se cumple ya que, no se cuenta con otro servidor que pueda reemplazar la cobertura del sistema mientras se instala alguna capacidad de hardware; por otro lado, en cuanto a los de software, tampoco se cumple ya que de igual forma sería necesario bajar la única instancia que se posee por lo que el sistema dejaría de estar disponible.

3- No lo cumple, ya que la información es enviada como JSON sin ningún tipo de protección a ninguno de los datos, dado que es local ni siquiera se utiliza protocolo HTTPS por lo que la información enviada es de texto plano.

4- No lo cumple, ya que no se administran los recursos de información y tampoco de servicios. Dado que se tiene solo una instancia del sistema y que no está pensado para ser replicado es imposible que se implente esta estrategia.

5- Si bien se realizan ciertas comprobaciones a nivel de front y back, no se cumple esta característica porque no es completamente tolerante a fallos, ni capaz de volver a un punto anterior y mucho menos capaz de derivar la operación ya que solamente funciona de forma local.

6- No lo cumple, ya que no se tienen diversos servidores o fuentes de datos. Las operaciones están pensadas para ser ejecutadas de forma secuencial y no paralela.

7- No lo cumple, ya que es posible identificar las rutas de get/post y el formato de la data como respuesta de estas solicitudes. No se detallarán las explicaciones de las otras ya que como se explicó antes es un sistema local que carece de características como concurrencia, replicación, migración, etc.

8- No lo cumple, ya que no se implementó ningún parámetro de seguridad, de patrón de diseño o algoritmo que mejore el rendimiento y tampoco de funciones o comprobaciones al nivel de una entrega fiable de datos.

Fuentes de información:

https://medium.com/@edusalguero/sistemas-distribuidos-caracterizacion-modelado-comunicacion-tiempo-2c1f85f4e67a

http://sistemas-distribuidos-unerg.blogspot.com/2008/10/caractersticas-principales-de-los.html

https://caracterizacionsd.wordpress.com/2013/04/05/caracterizacion-de-sistemas-distribuidos/#:~:text=Heterogeneidad%3A,ordenador%20y%20lenguajes%20de%20programaci%C3%B3n.&text=Internet%20permite%20que%20los%20usuarios,heterog%C3%A9neo%20de%20redes%20y%20computadores.

# Pep 2

A continuación se listarán todas las características que el sistema no cumple:

1- Heterogeneidad

2- Apertura

3- Seguridad

4- Escalabilidad

5- Manejo de fallos

6- Concurrencia

7- Transparencia

8- Calidad de servicio

Sin embargo es posible realizar ciertas modificaciones en la estructura del sistema original (Figura 1), para lograr abarcar cada una de las características mencionadas.
 
![alt text](/assets/original.JPG)

(Figura 1. Arquitectura original)


# Modificaciones en la estructura para cumplir los requerimientos:

1- En cuanto a la Heterogeneidad, se implementa en la estructura a través de un middleware en cada etapa de comunicación y/o conexión; así se permitirá poder trabajar la variedad de variables del sistema. 

2 y 4 - En cuanto a la Apertura y Escalabilidad, se implementa un administrador, el cual se encuentra constantemente revisando y dirirgiendo los servidores (similar a Zookeeper por ejemplo), de esta manera al detectar que un componente del sistema necesite expandirse este reasignará sus recursos según su necesidad. También cabe notar su facultad de generar n réplicas de la base de datos para el mismo fin antes explicado. También la implementación de microservicios, permitiría reforzar estos conceptos, ya que son fáciles de incluir en la arquitectura y permitirían añadir funcionalidades sin mayores problemas.

3- En cuanto a Seguridad, se implementa encriptación de los mensajes e información en cada conexión y respuesta. En particular se hizo uso de AES-256, el cual es un algoritmo de encriptación simétrico, teniendo que utilizar la misma clave tanto para encriptar como para desencriptar, lo que la hace muy segura. Todos el tráfico entre front y back se encuentra totalmente encriptado.

5- En cuanto al Manejo de fallos, se maneja un historial de estados de cada instancia del servidor con un servicio de rollback asociado, lo cual permitiría recuperar un estado anterior, de ser requerido.

6- En cuanto a la Concurrencia, esto ocurriría al tener la arquitectura presentada instanciada más de una vez, en ese caso los administradores de cada instancia se comunicarían entre ellos, manteniendo la sincronización entre sus mensajes. 

7 y 8 - Por último la Transparencia y Calidad del servicio, van de la mano con el cumplimiento de todas las anteriores, pero bajo la ética de que la forma en que se implementa contempla todas las situaciones y condiciones que pueden presentarse, así como la claridad en la forma en que opera el sistema sin ocultar la existencia de más de un servidor, etc.


![alt text](/assets/Arquitectura.jpg)
(Figura 2. Arquitectura modificada para cada característica)


# Arquitectura considerando la implementación de dos características:

Teniendo todo lo anterior en cuenta, para esta implementación, se elige la mejora de Apertura y Escalabilidad, orientándonos a la implementación de microservicios específicamente a través de docker; y por otro lado, el de seguridad encriptando los mensajes que se envían en cada etapa tal como se muestra en la imagen de la arquitectura final a implementar.


![alt text](/assets/Implementación.jpg)
(Figura 3. Arquitectura implementada para 2 características elegidas)
