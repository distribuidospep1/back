import Form from '../schemas/form.js';
import Sequelize from 'sequelize';
import { Router } from 'express';
import {encrypt, decrypt} from '../helpers/cryptoHelper.js'

const Op = Sequelize.Op;
const router = Router();

router.post('/validate', async (req, res) => {
  try {

    const encryptedData = req.body
    const decryptedData = decrypt(encryptedData)

    const {
      formId
    } = decryptedData

    let form = await Form.findOne({ where: { id: formId } });

    if (!form) {

      const toEncrypt = {       
        success: false,
        message: "Invalid indentifier",
      }
      
      const encryptedData = encrypt(toEncrypt)

      return res.json({
        encryptedData
      });
    }
    const timeNow = Date.now()
    const expirationDate = new Date(form.expiration_date)
    //console.log(expirationDate.getTime())
    //const timeLeft = expirationDate.getTime() - timeNow

    if (timeNow < expirationDate.getTime()) {
      const toEncrypt = {       
        success: true,
        message: `The permission is still valid`,
      }

      const encryptedData = encrypt(toEncrypt)

      return res.json({
        encryptedData
      });
    } else {

      const toEncrypt = {       
        success: true,
        message: `The permission is invalid`,
      }

      const encryptedData = encrypt(toEncrypt)

      return res.json({
        encryptedData
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong!"
    })
  }
});

export default router;