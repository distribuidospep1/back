import Form from '../schemas/form.js';
import Sequelize from 'sequelize';
import { Router } from 'express';
import strftime from 'strftime';
import { sendMail } from '../helpers/emailHelper.js';
import {encrypt, decrypt} from '../helpers/cryptoHelper.js'

const Op = Sequelize.Op;
const router = Router();

router.get('/getall', async (req, res) => {
  try {
    let getdata = await Form.findAll(req.body);
    if (getdata) {
      const toEncrypt = {       
        success: true,
        message: "User Fetch Successfully",
        data: getdata
      }
      const encryptedData = encrypt(toEncrypt)

      res.json({
        encryptedData
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong!"
    })
  }
});

router.post('/encryptData', async (req, res) => {
  const encryptedData = encrypt(req.body)
  return res.status(200).json({
    success: true,
    message: "Form encrypted successfully",
    data: encryptedData
  });

})

router.post('/create', async (req, res) => {
  try {

    const encryptedData = req.body
    const decryptedData = decrypt(encryptedData)
    const { rut, names, last_name, email, regionName, commune, address, street_number, reason } = decryptedData;

    const validation_date = Date.now() + (900000); // validated in 15 min
    const expiration_date = validation_date + (3 * 3600000); // expiration in 3 hours
    console.log(decryptedData)
    const form = await Form.create({ rut, names, last_name, region_name: regionName, commune,address, street_number, reason, email, validation_date, expiration_date });

    const str_validation_date = strftime('%F %T', new Date(validation_date));
    const str_expiration_date = strftime('%F %T', new Date(expiration_date));

    if (form) {

      await sendMail(form, str_validation_date, str_expiration_date)
        .catch(err => res.status(500).json({ message: err.message }));

      const data = {
        form,
        str_validation_date,
        str_expiration_date
      }

      const toEncrypt = {       
        success: true,
        message: "User Fetch Successfully",
        data: data
      }

      const encryptedData = encrypt(toEncrypt)

      res.json({
        encryptedData
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong!"
    })
  }
});

export default router;